# oasys (OA 自动化办公系统)

## 1. 项目介绍

oasys 是一个 OA 办公自动化系统，使用 Maven 进行项目管理，基于 SpringBoot 框架开发的项目，MySQL 底层数据库，前端采用 FreeMarker 模板引擎，Bootstrap 作为前端 UI 框架，集成了 jpa、mybatis 等框架。作为初学 SpringBoot 的同学是一个很不错的项目，如果想在此基础上面进行 OA 的增强，也是一个不错的方案。

初始版本来自 <https://gitee.com/aaluoxiang/oa_system>（[MIT license](https://gitee.com/aaluoxiang/oa_system/blob/master/LICENSE)）

## 2. 框架介绍

### 前端

| 技术 | 名称 | 版本 | 官网 |
| :--------: | :--------:| :--: |:--:|
| FreeMarker | 模板引擎 | SpringBoot 1.5.6.RELEASE 集成版本 | https://freemarker.apache.org/ |
| Bootstrap | 前端 UI 框架 | 3.3.7 | http://www.bootcss.com/ |
| jQuery | 快速的 JavaScript 框架 | 1.11.3 | https://jquery.com/ |
| KindEditor | HTML 可视化编辑器 | 4.1.10 | http://kindeditor.net |
| My97 DatePicker | 时间选择器 | 4.8 Beta4 | http://www.my97.net/ |

其他：highcharts、bootstrap、easyui、My97DatePicker、fullcalendar、kindeditor、jquery-ui、pjax、prefixfree、vue.js

### 后端

| 技术 | 名称 | 版本 | 官网 |
| :--------: | :--------:|:---:|:------:|
| SpringBoot | SpringBoot框架 | 1.5.6.RELEASE | https://spring.io/projects/spring-boot |
| JPA | spring-data-jpa | 1.5.6.RELEASE | https://projects.spring.io/spring-data-jpa |
| Mybatis | Mybatis 框架 | 1.3.0 | http://www.mybatis.org/mybatis-3 |
| Fastjson | json 解析包 | 1.2.36 | https://github.com/alibaba/fastjson |
| PageHelper | Mybatis 分页插件 | 1.0.0 | https://pagehelper.github.io |

## 3.部署流程

```bash
bash build.sh
java -jar target/oasys.jar
```

1. 下载项目、把 [oasys.sql](./oasys.sql) 导入本地数据库
2. 修改 [application.properties](./src/main/resources/application.properties)
3. 修改数据源，oasys ——> 自己本地的库名，用户名和密码修改成自己的
4. 修改相关路径，配置图片路径、文件路径、附件路径
5. OasysApplication.java 中的 main 方法运行，控制台没有报错信息，数据启动时间多久即运行成功
6. 在浏览器中输入 <http://localhost:8080>

## 4. 改进

- [x] 代码格式化（VSCode）
  - 安装 [prettier](https://github.com/jhipster/prettier-java)：`npm install -g prettier prettier-plugin-java`
  - 代码格式化：`prettier --parser=java --single-quote --print-width=140 --tab-width=4 --use-tabs=false --write "src/**/*.java"`
- [x] 密码 MD5 存储
- [x] 打包格式（原单一 jar，改成 jar + lib + config，支持外部配置文件）
- [ ] 静态资源外部托管
- [ ] 前后端分离（SPA static + API service）

## 5.项目截图

![代码示例.gif](https://images.gitee.com/uploads/images/2019/0927/141250_aeec4d38_1277461.gif "代码示例")
![操作展示.gif](https://images.gitee.com/uploads/images/2019/0927/141251_4ef0327c_1277461.gif "操作展示")
