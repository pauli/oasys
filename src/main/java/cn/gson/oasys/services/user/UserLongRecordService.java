package cn.gson.oasys.services.user;

import cn.gson.oasys.model.dao.user.UserLogRecordDao;
import cn.gson.oasys.model.entity.user.LoginRecord;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserLongRecordService {
    @Autowired
    private UserLogRecordDao ulDao;

    public LoginRecord save(LoginRecord lr) {
        return ulDao.save(lr);
    }
}
