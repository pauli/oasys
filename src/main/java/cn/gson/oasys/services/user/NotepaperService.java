package cn.gson.oasys.services.user;

import cn.gson.oasys.model.dao.processdao.NotepaperDao;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class NotepaperService {
    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private NotepaperDao ndao;

    @Value("${oasys.img.rootpath}")
    private String rootpath;

    public void delete(Long id) {
        ndao.delete(id);
    }

    @PostConstruct
    public void UserpanelController() {
        try {
            this.rootpath = ResourceUtils.getURL("classpath:").getPath().replace("target/classes/", "static/image");
        } catch (IOException e) {
            log.debug("获取项目路径异常: {}", this.rootpath);
        }
    }

    /**
     * 上传头像
     *
     * @throws IOException
     */
    public String upload(MultipartFile file) throws IOException {
        File dir = new File(this.rootpath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String fileName = file.getOriginalFilename();
        if (!StringUtils.isEmpty(fileName)) {
            String suffix = FilenameUtils.getExtension(fileName);
            String newFileName = UUID.randomUUID().toString().toLowerCase() + "." + suffix;
            File targetFile = new File(dir, newFileName);
            file.transferTo(targetFile);
            return targetFile.getPath().replace("\\", "/").replace(rootpath, "");
        }
        return null;
    }
}
