package cn.gson.oasys.api;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.gson.oasys.common.formvalid.BindingResultVOUtil;
import cn.gson.oasys.common.formvalid.ResultVO;
import cn.gson.oasys.model.dao.filedao.FileListdao;
import cn.gson.oasys.model.dao.filedao.FilePathdao;
import cn.gson.oasys.model.dao.user.UserDao;
import cn.gson.oasys.model.entity.file.FileList;
import cn.gson.oasys.model.entity.file.FilePath;
import cn.gson.oasys.model.entity.user.User;
import cn.gson.oasys.services.file.FileServices;

@RestController
@RequestMapping("/api")
public class FileApi {
    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private FileServices fs;

    @Autowired
    private FilePathdao fpdao;

    @Autowired
    private FileListdao fldao;

    @Autowired
    private UserDao udao;

    @PostMapping("/fileupload")
    public ResultVO fileupload( @RequestParam("file") MultipartFile file, HttpSession session) throws IOException {

        Long userid = Long.parseLong(session.getAttribute("userId") + "");
        User user = udao.findOne(userid);

        FilePath filepath = fpdao.findByPathName(user.getUserName());
        if (filepath == null) {
            filepath = new FilePath();
            filepath.setParentId(1L);
            filepath.setPathName(user.getUserName());
            filepath.setPathUserId(user.getUserId());
            filepath = fpdao.save(filepath);
        }
        // true 表示从文件使用上传
        FileList uploadfile = (FileList) fs.savefile(file, user, filepath, true);
        log.debug("{} upload file to: {}", user.getUserName(), uploadfile.getFilePath());
        return BindingResultVOUtil.success(uploadfile.getFileId());
    }
}
