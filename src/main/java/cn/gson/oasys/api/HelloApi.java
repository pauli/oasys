package cn.gson.oasys.api;

import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.gson.oasys.common.formvalid.BindingResultVOUtil;
import cn.gson.oasys.common.formvalid.ResultVO;

@RestController
@RequestMapping("/api")
public class HelloApi {

    @GetMapping("/hello")
    public ResultVO helloGet() throws IOException {
        return BindingResultVOUtil.success("hello api get");
    }

    @PostMapping("/hello")
    public ResultVO helloPost() throws IOException {
        return BindingResultVOUtil.success("hello api post");
    }
}
