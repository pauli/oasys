package cn.gson.oasys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import cn.gson.oasys.util.DateUtil;

@SpringBootApplication
public class OasysApplication {

    public static void main(String[] args) {
        Logger log = LoggerFactory.getLogger(OasysApplication.class);
        String now = DateUtil.now();
        log.info("system start on {}", now);
        try {
            SpringApplication.run(OasysApplication.class, args);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("system exit with error: {}", e.getMessage());
        }
    }
}
