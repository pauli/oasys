package cn.gson.oasys.controller.login;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(LoginConfig.class)
@ConfigurationProperties(prefix = LoginConfig.PREFIX, ignoreInvalidFields = true)
public class LoginConfig {
    public static final String PREFIX = "oasys.login"; // 这里对应配置文件中的前缀
    private String captcha;

    public void setCaptch(String captcha) {
        this.captcha = captcha;
    }

    public String getCaptch() {
        return this.captcha;
    }
}
