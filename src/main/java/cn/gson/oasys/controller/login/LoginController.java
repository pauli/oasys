package cn.gson.oasys.controller.login;

import cn.gson.oasys.model.dao.user.UserDao;
import cn.gson.oasys.model.entity.user.LoginRecord;
import cn.gson.oasys.model.entity.user.User;
import cn.gson.oasys.services.user.UserLongRecordService;
import cn.gson.oasys.util.SecretUtil;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class LoginController {
    @Autowired
    private UserDao uDao;

    @Autowired
    UserLongRecordService ulService;

    @Value("${oasys.login.captcha}")
    private String captcha = "none";

    public static final String CAPTCHA_KEY = "session_captcha";

    private Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 登录界面的显示
     *
     * @return
     */
    @GetMapping("login")
    public String login(Model model) {
        model.addAttribute("captcha", this.captcha);
        return "login/login";
    }

    @GetMapping("logout")
    public String logout(HttpSession session, Model model) {
        log.debug("logout: username={}", session.getAttribute("userId"));
        model.addAttribute("captcha", this.captcha);
        session.removeAttribute("userId");
        return "redirect:/login";
    }

    /**
     * 登录检查；
     * 1、根据(用户名或电话号码)+密码进行查找
     * 2、判断使用是否被冻结；
     *
     * @return
     * @throws UnknownHostException
     */
    @PostMapping("login")
    public String loginCheck(HttpSession session, HttpServletRequest req, Model model) throws UnknownHostException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        log.debug("loginCheck: username={}", userName);
        model.addAttribute("captcha", this.captcha);

        if (userName == null || password == null) {
            return "login/login";
        }

        userName = userName.trim().toLowerCase();

        // 图片验证码
        if ("image".equalsIgnoreCase(this.captcha)) {
            String ca = req.getParameter("code");
            String sesionCode = (String) req.getSession().getAttribute(CAPTCHA_KEY);
            model.addAttribute("userName", userName);
            if (ca == null || !ca.equalsIgnoreCase(sesionCode)) {
                model.addAttribute("errormess", "验证码输入错误!");
                req.setAttribute("errormess", "验证码输入错误!");
                return "login/login";
            }
        } else {
            log.debug("no captcha type config");
        }

        /*
         * 将用户名分开查找；用户名或者电话号码；
         */
        User user = uDao.findOneUser(userName);
        if (Objects.isNull(user) || !SecretUtil.comparePwd(password, user.getSalt(), user.getPassword())) {
            model.addAttribute("errormess", "账号或密码错误!");
            return "login/login";
        }
        if (user.getIsLock() == 1) {
            model.addAttribute("errormess", "账号已被冻结!");
            return "login/login";
        }
        Object sessionId = session.getAttribute("userId");
        if (sessionId != null && (Long) sessionId == user.getUserId()) {
            model.addAttribute("hasmess", "当前用户已经登录了；不能重复登录");
            session.setAttribute("thisuser", user);
            return "login/login";
        } else {
            session.setAttribute("userId", user.getUserId());
            Browser browser = UserAgent.parseUserAgentString(req.getHeader("User-Agent")).getBrowser();
            Version version = browser.getVersion(req.getHeader("User-Agent"));
            String info = browser.getName() + "/" + version.getVersion();
            String ip = InetAddress.getLocalHost().getHostAddress();
            /*新增登录记录*/
            ulService.save(new LoginRecord(ip, new Date(), info, user));
        }
        return "redirect:/";
    }

    @GetMapping("handlehas")
    public String handleHas(HttpSession session, Model model) {
        model.addAttribute("captcha", this.captcha);
        if (!StringUtils.isEmpty(session.getAttribute("thisuser"))) {
            User user = (User) session.getAttribute("thisuser");
            session.removeAttribute("userId");
            session.setAttribute("userId", user.getUserId());
        } else {
            return "login/login";
        }
        return "redirect:/";
    }

    @GetMapping("captcha")
    public void captcha(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
        log.debug("captcha init start");

        if ("image".equalsIgnoreCase(this.captcha)) {
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");
            response.setDateHeader("Expires", 0);
            response.setContentType("image/jpeg");

            // 生成随机字串
            String verifyCode = VerifyCodeUtils.generateVerifyCode(4);

            // 将验证码存储在session以便登录时校验
            session.setAttribute(CAPTCHA_KEY, verifyCode.toLowerCase());

            // 生成图片
            int w = 135, h = 40;
            OutputStream out = response.getOutputStream();
            VerifyCodeUtils.outputImage(w, h, out, verifyCode);
            out.flush();
            out.close();
        } else {
            PrintWriter out = response.getWriter();
            out.println("no captcha type config");
            out.flush();
            out.close();
        }
        log.debug("captcha init end");
    }
}
