package cn.gson.oasys.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/healthcheck")
public class HealthCheckController {

    @GetMapping("")
    public Map<String, Object> index(HttpServletRequest req) {
        Object name = req.getParameter("name");
        Map<String, Object> result = new HashMap<>();
        result.put("name", name);
        return result;
    }
}
