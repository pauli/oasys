package cn.gson.oasys.controller.file;

import cn.gson.oasys.model.dao.filedao.FileListdao;
import cn.gson.oasys.model.dao.filedao.FilePathdao;
import cn.gson.oasys.model.dao.user.UserDao;
import cn.gson.oasys.model.entity.file.FileList;
import cn.gson.oasys.model.entity.file.FilePath;
import cn.gson.oasys.model.entity.user.User;
import cn.gson.oasys.services.file.FileServices;
import cn.gson.oasys.services.file.FileTransactionalHandlerService;
import cn.gson.oasys.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/")
public class FileController {
    Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private FileServices fs;

    @Autowired
    private FilePathdao fpdao;

    @Autowired
    private FileListdao fldao;

    @Autowired
    private UserDao udao;

    @Autowired
    private FileTransactionalHandlerService fileTransactionalHandlerService;

    /**
     * 第一次进入
     *
     * @param model
     * @return
     */
    @RequestMapping("filemanage")
    public String usermanage(@SessionAttribute("userId") Long userid, Model model) {
        User user = udao.findOne(userid);
        FilePath filepath = fpdao.findByPathName(user.getUserName());
        if (filepath == null) {
            FilePath filepath1 = new FilePath();
            filepath1.setParentId(1L);
            filepath1.setPathName(user.getUserName());
            filepath1.setPathUserId(user.getUserId());
            filepath = fpdao.save(filepath1);
        }

        model.addAttribute("nowpath", filepath);
        model.addAttribute("paths", fs.findpathByparent(filepath.getId()));
        model.addAttribute("files", fs.findfileBypath(filepath));
        model.addAttribute("userrootpath", filepath);
        model.addAttribute("mcpaths", fs.findpathByparent(filepath.getId()));
        return "file/filemanage";
    }

    /**
     * 进入指定文件夹 的controller方法
     *
     * @param pathid
     * @param model
     * @return
     */
    @RequestMapping("filetest")
    public String text(@SessionAttribute("userId") Long userid, @RequestParam("pathid") Long pathid, Model model) {
        User user = udao.findOne(userid);
        FilePath userrootpath = fpdao.findByPathName(user.getUserName());

        // 查询当前目录
        FilePath filepath = fpdao.findOne(pathid);

        // 查询当前目录的所有父级目录
        List<FilePath> allparentpaths = new ArrayList<>();
        fs.findAllParent(filepath, allparentpaths);
        Collections.reverse(allparentpaths);

        model.addAttribute("allparentpaths", allparentpaths);
        model.addAttribute("nowpath", filepath);
        model.addAttribute("paths", fs.findpathByparent(filepath.getId()));
        model.addAttribute("files", fs.findfileBypath(filepath));
        //复制移动显示 目录
        model.addAttribute("userrootpath", userrootpath);
        model.addAttribute("mcpaths", fs.findpathByparent(userrootpath.getId()));
        return "file/filemanage";
    }

    /**
     * 文件上传 controller方法
     *
     * @param file
     * @param pathid
     * @param session
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("fileupload")
    public String uploadfile(
            @RequestParam("file") MultipartFile file,
            @RequestParam("pathid") Long pathid,
            HttpSession session,
            Model model
    )
            throws IOException {
        Long userid = Long.parseLong(session.getAttribute("userId") + "");
        User user = udao.findOne(userid);
        FilePath nowpath = fpdao.findOne(pathid);
        // true 表示从文件使用上传
        FileList uploadfile = (FileList) fs.savefile(file, user, nowpath, true);
        log.debug("{} upload file to: {}", user.getUserName(), uploadfile.getFilePath());
        model.addAttribute("pathid", pathid);
        return "forward:/filetest";
    }

    /**
     * 文件分享
     *
     * @param pathid
     * @param checkfileids
     * @param model
     * @return
     */
    @RequestMapping("doshare")
    public String doshare(@RequestParam("pathid") Long pathid, @RequestParam("checkfileids") List<Long> checkfileids, Model model) {
        if (!checkfileids.isEmpty()) {
            fs.doshare(checkfileids);
        }
        model.addAttribute("pathid", pathid);
        model.addAttribute("message", "分享成功");
        return "forward:/filetest";
    }

    /**
     * 删除前台选择的文件以及文件夹
     *
     * @param pathid
     * @param checkpathids
     * @param checkfileids
     * @param model
     * @return
     */
    @RequestMapping("deletefile")
    public String deletefile(
            @SessionAttribute("userId") Long userid,
            @RequestParam("pathid") Long pathid,
            @RequestParam("checkpathids") List<Long> checkpathids,
            @RequestParam("checkfileids") List<Long> checkfileids,
            Model model
    ) {
        if (!checkfileids.isEmpty()) {
            //文件放入回收战
            fileTransactionalHandlerService.trashfile(checkfileids, 1L, userid);
        }
        if (!checkpathids.isEmpty()) {
            fs.trashpath(checkpathids, 1L, true);
        }

        model.addAttribute("pathid", pathid);
        return "forward:/filetest";
    }

    /**
     * 重命名
     *
     * @param name
     * @param renamefp
     * @param pathid
     * @param model
     * @return
     */

    @RequestMapping("rename")
    public String rename(
            @RequestParam("name") String name,
            @RequestParam("renamefp") Long renamefp,
            @RequestParam("pathid") Long pathid,
            @RequestParam("isfile") boolean isfile,
            Model model
    ) {
        //这里调用重命名方法
        fs.rename(name, renamefp, pathid, isfile);

        model.addAttribute("pathid", pathid);
        return "forward:/filetest";
    }

    /**
     * 移动和复制
     *
     * @param mctoid
     * @param model
     * @return
     */
    @RequestMapping("mcto")
    public String mcto(
            @SessionAttribute("userId") Long userid,
            @RequestParam("morc") boolean morc,
            @RequestParam("mctoid") Long mctoid,
            @RequestParam("pathid") Long pathid,
            @RequestParam("mcfileids") List<Long> mcfileids,
            @RequestParam("mcpathids") List<Long> mcpathids,
            Model model
    ) {
        if (morc) {
            log.debug("这里是移动！~~");
            fs.moveAndcopy(mcfileids, mcpathids, mctoid, true, userid);
        } else {
            log.debug("这里是复制！~~");
            fs.moveAndcopy(mcfileids, mcpathids, mctoid, false, userid);
        }
        model.addAttribute("pathid", pathid);
        return "forward:/filetest";
    }

    /**
     * 新建文件夹
     *
     * @param pathid
     * @param pathname
     * @param model
     * @return
     */
    @RequestMapping("createpath")
    public String createpath(
            @SessionAttribute("userId") Long userid,
            @RequestParam(value = "pathid", required = true) Long pathid,
            @RequestParam("pathname") String pathname,
            Model model
    ) {
        FilePath filepath = fpdao.findOne(pathid);
        String newname = fs.onlyname(pathname, filepath, null, 1, false);
        FilePath newfilepath = new FilePath(pathid, newname);
        newfilepath.setPathUserId(userid);
        fpdao.save(newfilepath);
        model.addAttribute("pathid", pathid);
        return "forward:/filetest";
    }

    /**
     * 图片预览
     *
     * @param response
     * @param fileid
     */
    @GetMapping("imgshow")
    public void imgshow(HttpServletResponse response, @RequestParam(value = "fileid", required = true) Long fileid) throws IOException {
        FileList filelist = fldao.findOne(fileid);
        File file = fs.getFile(filelist.getFilePath());
        FileUtil.writeFileToStream(response, file);
    }

    /**
     * 下载文件
     *
     * @param response
     * @param fileid
     */
    @RequestMapping("downfile")
    public void downFile(HttpServletResponse response, @RequestParam(value = "fileid", required = true) Long fileid) throws IOException {
        FileList filelist = fldao.findOne(fileid);
        File file = fs.getFile(filelist.getFilePath());
        response.setContentLength(filelist.getSize().intValue());
        response.setContentType(filelist.getContentType());
        response.setHeader(
                "Content-Disposition",
                "attachment;filename=" + new String(filelist.getFileName().getBytes("UTF-8"), "ISO8859-1")
        );
        FileUtil.writeFileToStream(response, file);
    }

    @RequestMapping("mcloadpath")
    public String mcloadpath(@RequestParam("mctoid") Long mctoid, @RequestParam("mcpathids") List<Long> mcpathids, Model model) {
        List<FilePath> showsonpaths = fs.mcpathload(mctoid, mcpathids);
        model.addAttribute("mcpaths", showsonpaths);
        return "file/mcpathload";
    }

    /**
     * 文件类型筛选显示load
     *
     * @param userid
     * @param type
     * @param model
     * @return
     */
    @RequestMapping("filetypeload")
    public String filetypeload(@SessionAttribute("userId") Long userid, @RequestParam("type") String type, Model model) {
        User user = udao.findOne(userid);
        String contenttype;
        List<FileList> fileLists = null;
        List<FilePath> filePaths = null;
        switch (type) {
            case "document":
                fileLists = fldao.finddocument(user);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "picture":
                contenttype = "image/%";
                fileLists = fldao.findByUserAndContentTypeLikeAndFileIstrash(user, contenttype, 0L);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "music":
                contenttype = "audio/%";
                fileLists = fldao.findByUserAndContentTypeLikeAndFileIstrash(user, contenttype, 0L);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "video":
                contenttype = "video/%";
                fileLists = fldao.findByUserAndContentTypeLikeAndFileIstrash(user, contenttype, 0L);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "yasuo":
                contenttype = "application/x%";
                fileLists = fldao.findByUserAndContentTypeLikeAndFileIstrash(user, contenttype, 0L);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "trash":
                filePaths = fpdao.findByPathUserIdAndPathIstrash(userid, 1L);
                fileLists = fldao.findByUserAndFileIstrash(user, 1L);

                model.addAttribute("paths", filePaths);
                model.addAttribute("files", fileLists);
                model.addAttribute("istrash", 1);
                model.addAttribute("isload", 1);
                break;
            case "share":
                fileLists = fldao.findByFileIsshareAndFileIstrash(1L, 0L);
                model.addAttribute("files", fileLists);
                model.addAttribute("isshare", 1);
                model.addAttribute("isload", 1);
                model.addAttribute("userid", userid);
                break;
            default:
                break;
        }

        model.addAttribute("type", type);
        return "file/filetypeload";
    }

    @RequestMapping("findfileandpath")
    public String findfileandpath(
            @SessionAttribute("userId") Long userid,
            @RequestParam(value = "findfileandpath", required = false) String findfileandpath,
            @RequestParam(value = "type", defaultValue = "all") String type,
            Model model
    ) {
        String findlike = "%" + findfileandpath + "%";
        User user = udao.findOne(userid);
        FilePath fpath = fpdao.findByParentIdAndPathUserId(1L, userid);
        String contenttype;
        List<FileList> fileLists = null;
        List<FilePath> filePaths = null;
        switch (type) {
            case "document":
                fileLists = fldao.finddocumentlike(user, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "picture":
                contenttype = "image/%";
                fileLists = fldao.findByUserAndFileIstrashAndContentTypeLikeAndFileNameLike(user, 0L, contenttype, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "music":
                contenttype = "audio/%";
                fileLists = fldao.findByUserAndFileIstrashAndContentTypeLikeAndFileNameLike(user, 0L, contenttype, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "video":
                contenttype = "video/%";
                fileLists = fldao.findByUserAndFileIstrashAndContentTypeLikeAndFileNameLike(user, 0L, contenttype, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "yasuo":
                contenttype = "application/x%";
                fileLists = fldao.findByUserAndFileIstrashAndContentTypeLikeAndFileNameLike(user, 0L, contenttype, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("isload", 1);
                break;
            case "trash":
                filePaths = fpdao.findByPathUserIdAndPathIstrashAndPathNameLikeAndParentIdNot(userid, 1L, findlike, 1L);
                fileLists = fldao.findByUserAndFileIstrashAndContentTypeLikeAndFileNameLike(user, 1L, "%%", findlike);
                model.addAttribute("istrash", 1);
                model.addAttribute("isload", 1);
                model.addAttribute("paths", filePaths);
                model.addAttribute("files", fileLists);
                break;
            case "share":
                fileLists = fldao.findByFileIsshareAndFileNameLike(1L, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("isshare", 1);
                model.addAttribute("isload", 1);
                break;
            default:
                filePaths = fpdao.findByPathUserIdAndPathIstrashAndPathNameLikeAndParentIdNot(userid, 0L, findlike, 1L);
                fileLists = fldao.findByUserAndFileIstrashAndFileNameLike(user, 0L, findlike);
                model.addAttribute("files", fileLists);
                model.addAttribute("paths", filePaths);
                model.addAttribute("isload", 1);
                break;
        }

        model.addAttribute("type", type);
        return "file/filetypeload";
    }

    @RequestMapping("fileloadshare")
    public String fileloadshare(
            @RequestParam("type") String type,
            @RequestParam(value = "checkfileids[]", required = false) List<Long> checkfileids,
            Model model
    ) {
        if (checkfileids != null) {
            fs.doshare(checkfileids);
        }

        model.addAttribute("message", "分享成功");
        model.addAttribute("type", type);
        return "forward:/filetypeload";
    }

    /**
     * load删除controller
     *
     * @param type
     * @param checkpathids
     * @param checkfileids
     * @param model
     * @return
     */
    @RequestMapping("fileloaddeletefile")
    public String fileloaddeletefile(
            @RequestParam("type") String type,
            @RequestParam(value = "checkpathids[]", required = false) List<Long> checkpathids,
            @RequestParam(value = "checkfileids[]", required = false) List<Long> checkfileids,
            Model model
    ) {
        if (checkfileids != null) {
            // 删除文件
            fs.deleteFile(checkfileids);
        }
        if (checkpathids != null) {
            // 删除文件夹
            fs.deletePath(checkpathids);
        }

        model.addAttribute("type", type);
        return "forward:/filetypeload";
    }

    /**
     * 将文件放入回收战
     *
     * @param userid
     * @param type
     * @param checkpathids
     * @param checkfileids
     * @param model
     * @return
     */
    @RequestMapping("fileloadtrashfile")
    public String fileloadtrashfile(
            @SessionAttribute("userId") Long userid,
            @RequestParam("type") String type,
            @RequestParam(value = "checkpathids[]", required = false) List<Long> checkpathids,
            @RequestParam(value = "checkfileids[]", required = false) List<Long> checkfileids,
            Model model
    ) {
        if (checkfileids != null) {
            // 文件放入回收站
            fileTransactionalHandlerService.trashfile(checkfileids, 1L, userid);
        }
        if (checkpathids != null) {
            // 删除文件夹
            fs.trashpath(checkpathids, 1L, true);
        }

        model.addAttribute("type", type);
        return "forward:/filetypeload";
    }

    /**
     * load重命名
     *
     * @param type
     * @param renamefp
     * @param creatpathinput
     * @param isfile
     * @param pathid
     * @param model
     * @return
     */
    @RequestMapping("fileloadrename")
    public String fileloadrename(
            @RequestParam("type") String type,
            @RequestParam("renamefp") Long renamefp,
            @RequestParam("creatpathinput") String creatpathinput,
            @RequestParam("isfile") boolean isfile,
            @RequestParam(value = "pathid", required = false) Long pathid,
            Model model
    ) {
        fs.rename(creatpathinput, renamefp, pathid, isfile);
        model.addAttribute("type", type);
        return "forward:/filetypeload";
    }

    /**
     * 回收站load 复原
     *
     * @param userid
     * @param type
     * @param checkpathids
     * @param checkfileids
     * @param model
     * @return
     */
    @RequestMapping("filereturnback")
    public String filereturnback(
            @SessionAttribute("userId") Long userid,
            @RequestParam("type") String type,
            @RequestParam(value = "checkpathids[]", required = false) List<Long> checkpathids,
            @RequestParam(value = "checkfileids[]", required = false) List<Long> checkfileids,
            Model model
    ) {
        if (checkfileids != null) {
            fileTransactionalHandlerService.filereturnback(checkfileids, userid);
        }
        if (checkpathids != null) {
            fs.pathreturnback(checkpathids, userid);
        }

        model.addAttribute("type", type);
        return "forward:/filetypeload";
    }
}
