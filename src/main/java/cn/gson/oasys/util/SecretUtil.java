package cn.gson.oasys.util;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

import org.springframework.util.DigestUtils;

public class SecretUtil {

    private SecretUtil() {
    }

    /**
     * md5 加密
     *
     * @param origin
     * @param salt
     * @return
     */
    public static String md5Encrypt(String origin, String salt) {
        if (origin == null || "".equals(origin.trim())) {
            origin = "123456"; // 初始密码
        }
        return DigestUtils.md5DigestAsHex((origin + "-oasys-" + salt).getBytes());
    }

    public static boolean comparePwd(String origin, String salt, String db) {
        return SecretUtil.md5Encrypt(origin, salt).equalsIgnoreCase(db);
    }

    /**
     * 生成 salt 只包含字母和数字
     * https://developer.aliyun.com/ask/61890
     * https://community.oracle.com/blogs/evanx/2013/01/24/password-salt
     *
     * @return
     */
    public static String genSalt() {
        Random ramdom = new SecureRandom();
        byte[] salt = new byte[16];
        ramdom.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt).substring(0, 8).toLowerCase();
    }
}
