package cn.gson.oasys.util;

import com.google.gson.Gson;

public class StringUtil {
    private StringUtil() {
    }

    public static String toJson(Object str) {
        return new Gson().toJson(str);
    }
}
