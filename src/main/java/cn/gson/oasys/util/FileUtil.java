package cn.gson.oasys.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public class FileUtil {
    private FileUtil() {
    }

    /**
     * 写文件 方法
     *
     * @param response
     * @param file
     * @throws IOException
     */
    public static void writeFileToStream(HttpServletResponse response, File file) throws IOException {
        if (file == null || !file.exists()) {
            IOUtils.write("Not Found!".getBytes(), response.getOutputStream());
            response.sendError(404);
            response.setStatus(404);
            response.getOutputStream().close();
        } else {
            FileUtil.writeFileToStream(response.getOutputStream(), file);
        }
    }

    /**
     * 写文件 方法
     *
     * @param response
     * @param file
     * @throws IOException
     */
    public static void writeFileToStream(OutputStream os, File file) throws IOException {
        try (FileInputStream aa = new FileInputStream(file)){
            // 读取文件问字节码
            byte[] data = new byte[(int) file.length()];
            IOUtils.readFully(aa, data);
            // 将文件流输出到浏览器
            IOUtils.write(data, os);
        } finally {
            os.close();
        }
    }
}
