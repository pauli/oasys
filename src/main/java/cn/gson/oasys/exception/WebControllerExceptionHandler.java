package cn.gson.oasys.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.service.spi.ServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class WebControllerExceptionHandler {
    Logger log = LoggerFactory.getLogger(getClass());
    /**
     * 全局异常捕捉处理
     * @param ex
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public void errorHandler(Exception ex) {
        log.error("全局异常处理：{}", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = ServiceException.class)
    public void errorHandler(ServiceException ex) {
        log.error("业务异常处理：{}", ex.getMessage());
    }
}
