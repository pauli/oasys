package cn.gson.oasys.model.dao.notedao;

import cn.gson.oasys.model.entity.note.Director;
import cn.gson.oasys.model.entity.note.Note;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DirectorDao extends PagingAndSortingRepository<Director, Long> {
}
