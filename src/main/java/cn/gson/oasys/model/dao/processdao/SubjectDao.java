package cn.gson.oasys.model.dao.processdao;

import cn.gson.oasys.model.entity.process.Subject;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubjectDao extends PagingAndSortingRepository<Subject, Long> {
    List<Subject> findByParentId(Long id);

    List<Subject> findByParentIdNot(Long id);
}
