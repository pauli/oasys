package cn.gson.oasys.model.dao.processdao;

import cn.gson.oasys.model.entity.process.EvectionMoney;
import cn.gson.oasys.model.entity.process.Stay;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface StayDao extends PagingAndSortingRepository<Stay, Long> {
    List<Stay> findByEvemoney(EvectionMoney money);
}
