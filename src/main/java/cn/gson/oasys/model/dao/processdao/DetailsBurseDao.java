package cn.gson.oasys.model.dao.processdao;

import cn.gson.oasys.model.entity.process.Bursement;
import cn.gson.oasys.model.entity.process.DetailsBurse;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface DetailsBurseDao extends PagingAndSortingRepository<DetailsBurse, Long> {
    List<DetailsBurse> findByBurs(Bursement bu);
}
