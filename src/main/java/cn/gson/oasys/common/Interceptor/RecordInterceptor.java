package cn.gson.oasys.common.interceptor;

import cn.gson.oasys.util.BeanUtil;
import cn.gson.oasys.common.formvalid.BindingResultVOUtil;
import cn.gson.oasys.model.dao.roledao.RolepowerlistDao;
import cn.gson.oasys.model.dao.system.SystemMenuDao;
import cn.gson.oasys.model.dao.user.UserDao;
import cn.gson.oasys.model.dao.user.UserLogDao;
import cn.gson.oasys.model.entity.role.Rolemenu;
import cn.gson.oasys.model.entity.system.SystemMenu;
import cn.gson.oasys.model.entity.user.User;
import cn.gson.oasys.model.entity.user.UserLog;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class RecordInterceptor extends HandlerInterceptorAdapter {
    Logger log = LoggerFactory.getLogger(getClass());

    /**
     * 在方法被调用前执行。在该方法中可以做类似校验的功能。
     * 如果返回true，则继续调用下一个拦截器。
     * 如果返回false，则中断执行，也就是说我们想调用的方法不会被执行，但是你可以修改response为你想要的响应。
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        boolean isApi = request.getRequestURI().startsWith("/api");

        log.debug("RecordInterceptor.preHandle: {}", request.getRequestURI());

        if (isApi) {
            return true;
        }

        if (!StringUtils.isEmpty(session.getAttribute("userId"))) {
            //导入dao类
            UserDao udao = BeanUtil.getBean(UserDao.class, request);
            RolepowerlistDao rpdao = BeanUtil.getBean(RolepowerlistDao.class, request);
            Long uid = Long.parseLong(session.getAttribute("userId") + "");
            User user = udao.findOne(uid);
            List<Rolemenu> oneMenuAll = rpdao.findbyparentxianall(0L, user.getRole().getRoleId(), true, false);
            List<Rolemenu> twoMenuAll = rpdao.findbyparentsxian(0L, user.getRole().getRoleId(), true, false);
            List<Rolemenu> all = new ArrayList<>();
            //获取当前访问的路径
            String url = request.getRequestURL().toString();
            String zhuan = "notlimit";

            if (oneMenuAll.size() > 0) {
                all.addAll(oneMenuAll);
            }
            if (twoMenuAll.size() > 0) {
                all.addAll(twoMenuAll);
            }
            for (Rolemenu rolemenu : all) {
                if (!rolemenu.getMenuUrl().equals(url)) {
                    return true;
                } else {
                    request.getRequestDispatcher(zhuan).forward(request, response);
                }
            }
        } else {
            response.sendRedirect("/login");
            return false;
        }

        return super.preHandle(request, response, handler);
    }

    /**
     * 在方法被调用前执行。在该方法中可以做类似校验的功能。
     * 如果返回true，则继续调用下一个拦截器。
     * 如果返回false，则中断执行，也就是说我们想调用的方法不会被执行，但是你可以修改response为你想要的响应。
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.debug("RecordInterceptor.postHandle: {}", request.getRequestURI());

        if (request.getRequestURI().startsWith("/api") && response.getStatus() != 200) {
            IOUtils.write(BindingResultVOUtil.error(response.getStatus(), "").toString().getBytes(), response.getOutputStream());
            response.getOutputStream().close();
        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        log.debug("RecordInterceptor.afterCompletion: {}", request.getRequestURI());
        HttpSession session = request.getSession();
        //导入dao类
        UserDao userDao = BeanUtil.getBean(UserDao.class, request);
        SystemMenuDao systemMenuDao = BeanUtil.getBean(SystemMenuDao.class, request);
        UserLogDao userLogDao = BeanUtil.getBean(UserLogDao.class, request);

        UserLog uLog = new UserLog();
        //首先就获取ip
        InetAddress ia = null;
        ia = ia.getLocalHost();
        String ip = ia.getHostAddress();
        uLog.setIpAddr(ip);
        uLog.setUrl(request.getServletPath());
        uLog.setLogTime(new Date());

        // 还没有登陆不能获取session
        Long id = (Long) session.getAttribute("id");
        if (StringUtils.isEmpty(id)) {
            return;
        }
        uLog.setUser(userDao.findOne(id));
        //从菜单表里面匹配
        List<SystemMenu> sMenus = (List<SystemMenu>) systemMenuDao.findAll();
        for (SystemMenu systemMenu : sMenus) {
            if (systemMenu.getMenuUrl().equals(request.getServletPath())) {
                //只有当该记录的路径不等于第一条的时候
                if (!userLogDao.findByUserlaset(1l).getUrl().equals(systemMenu.getMenuUrl())) {
                    uLog.setTitle(systemMenu.getMenuName());
                    //只要匹配到一个保存咯
                    userLogDao.save(uLog);
                }
            }
        }
    }
}
