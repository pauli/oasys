package cn.gson.oasys.common.formvalid;

/**
 * Description:
 * http请求返回的最外层对象
 *
 * @author zzy
 * @create 2017-09-07 13:38
 */

public class ResultVO<T> {
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 返回的具体内容
     */
    T data;


    public ResultVO() {
    }

    public ResultVO(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultVO(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultVO<T> success(T data) {
        this.code = ResultEnum.SUCCESS.getCode();
        this.msg = ResultEnum.SUCCESS.getMessage();
        this.data = data;
        return new ResultVO<>(code, msg, data);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResultVO<T> error(Integer code, String message, T data) {
        this.code = ResultEnum.ERROR.getCode();
        this.msg = message;
        this.data = data;
        return new ResultVO<>(code, msg, data);
    }

    @Override
    public String toString() {
        return "{\"code\":" + this.code + ",\"msg\":\"" + this.msg + "\",\"data\":\"" + this.data + "\"}";
    }
}
