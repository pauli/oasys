package cn.gson.oasys.common.formvalid;

/**
 * Description:
 * 异常结果抛出
 *
 * @author zzy
 * @create 2017-09-12 14:56
 */
public enum ResultEnum {
    ERROR(201, "auth failed"),
    SUCCESS(200, "success"),
    NONETYPE(211, "missing parameter");

    private Integer code;
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
