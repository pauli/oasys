package cn.gson.oasys.common.formvalid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class BindingResultVOUtil {

    private BindingResultVOUtil() {
    }

    /**
     * 表单验证，返回形式ResultVO
     *
     * @param br
     * @return
     */
    public static ResultVO<Object> hasErrors(BindingResult br) {
        if (br.hasErrors()) {
            List<FieldError> fieldErrors = br.getFieldErrors();
            List<String> messagge;
            Map<String, List<String>> map = new HashMap<>();
            for (FieldError fieldError : fieldErrors) {
                if (!map.containsKey(fieldError.getField())) {
                    messagge = new ArrayList<>();
                } else {
                    messagge = map.get(fieldError.getField());
                }
                messagge.add(fieldError.getDefaultMessage());
                map.put(fieldError.getField(), messagge);
            }
            return BindingResultVOUtil.verifyError(ResultEnum.ERROR.getCode(), ResultEnum.ERROR.getMessage(), map);
        }
        return BindingResultVOUtil.success();
    }

    public static ResultVO<Object> success() {
        return success(null);
    }

    public static ResultVO<Object> success(Object object) {
        ResultVO<Object> resultVO = new ResultVO<>();
        resultVO.setData(object);
        resultVO.setMsg(ResultEnum.SUCCESS.getMessage());
        resultVO.setCode(ResultEnum.SUCCESS.getCode());
        return resultVO;
    }

    public static ResultVO<Object> error(Integer code, String msg) {
        ResultVO<Object> resultVo = new ResultVO<>();
        resultVo.setCode(code);
        resultVo.setMsg(msg);
        return resultVo;
    }

    /**
     * 验证错误
     *
     * @param code
     * @param msg
     * @param map
     * @return
     */
    public static ResultVO<Object> verifyError(Integer code, String msg, Map<String, List<String>> map) {
        ResultVO<Object> resultVo = new ResultVO<>();
        resultVo.setCode(code);
        resultVo.setMsg(msg);
        resultVo.setData(map);
        return resultVo;
    }
}
