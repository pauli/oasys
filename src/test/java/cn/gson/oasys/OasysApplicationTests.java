package cn.gson.oasys;

import static org.junit.Assert.assertEquals;

import cn.gson.oasys.model.entity.notice.NoticesList;
import cn.gson.oasys.services.inform.InformService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OasysApplicationTests {
    @Autowired
    private InformService informService;

    @Test
    public void testFengZhuang() {
        List<NoticesList> noticelist = new ArrayList<>();
        List<Map<String, Object>> list = informService.fengZhuang(noticelist);
        for (Map<String, Object> map : list) {
            System.out.println(map);
        }
        assertEquals("testFengZhuang", noticelist, list);
    }
}
