package cn.gson.oasys;

import static org.junit.Assert.assertEquals;

import cn.gson.oasys.util.SecretUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SecretTest {

    @Test
    public void md5Encrypt() {
        String origin = "123456";
        String salt = "8888";
        String md5 = SecretUtil.md5Encrypt(origin, salt);
        System.out.printf("origin=%, salt=%, md5=%", origin, salt, md5);
        assertEquals("34130c31a598f46aa53c3e2f7649cf4c", "xxxxx", md5);
    }

    public static void main(String args[]) {
        String password = "123456";
        String salt = "8888";
        String encrypt = SecretUtil.md5Encrypt(password, salt);
        System.out.println(encrypt);
    }
}
