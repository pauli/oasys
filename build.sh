#!/bin/bash

PRJ_DIR=$(cd `dirname $0`; pwd)
echo PRJ_DIR=$PRJ_DIR
cd $PRJ_DIR

rm -rf logs .history target *.log

mvn clean install

mkdir -p target/config
cp -f src/main/resources/application.properties target/config
cp -f src/main/resources/logback.xml target/config

cd target
rm -rf archive-tmp generated-sources maven-archiver classes generated-test-sources maven-status oasys.jar.original test-classes
