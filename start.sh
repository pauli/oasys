#!/bin/bash

PRJ_DIR=$(cd `dirname $0`; pwd)
echo PRJ_DIR=$PRJ_DIR
cd $PRJ_DIR

rm -rf logs *.log

nohup java -Xms256m -Xmx512m -XX:PermSize=128M -XX:MaxPermSize=256M  -jar oasys.jar >> server.log 2>&1 &
